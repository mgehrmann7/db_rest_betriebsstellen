package DB.rest;

import org.json.simple.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller für die Rückgabe der Daten von Betriebsstellen über
 * REST-Endpoint.
 *
 * @author Marcel Gehrmann
 */
@RestController
public class BetriebsstelleController {

    /**
     * Allgemeine Rückgabevariante für alle Version mit selbstgenerierter
     * ResponseEntity.
     *
     * @param abk - Abkürzung der Betriebsstelle
     * @return ResponseEntity mit selbstgenerierter Rückgabe
     */
    @GetMapping("/betriebsstelle/{abk}")
    public ResponseEntity<String> betriebsstelle(@PathVariable String abk) {

        //Response Builder
        JSONObject content = BetriebsstellenREST.convertedData.get(abk.toUpperCase());

        String response = "HTTP-STATUS: 200" + "\n{\n";
        for (Object key : content.keySet()) {
            response += "\t" + (String) key + ": " + content.get(key) + "\n";
        }
        response += "}";

        // Return
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    // Alternative, versionsunabhängige Variante
    /**
     * Alternative Rückgabevariante, durch welche eine automatisch generierte
     * Antwort geliefert wird.
     *
     * @param abk - Abkürzung der Betriebsstelle
     * @return Betriebsstelle als Objekt
     */
    @GetMapping("/betriebsstelle_alt/{abk}")
    public Betriebsstelle betriebsstelle_alt(@PathVariable String abk) {
        return new Betriebsstelle(BetriebsstellenREST.convertedData.get(abk.toUpperCase()));
    }

    // Alternative, versionsspezifische Varianten
    /**
     * Alternative, Rückgabevariante, durch welche eine automatisch generierte
     * Antwort basierend auf der 2015er-Version geliefert wird.
     *
     * @param abk - Abkürzung der Betriebsstelle
     * @return Betriebsstelle als Objekt in 2015er-Variante
     */
    @GetMapping("/betriebsstelle2015/{abk}")
    public Betriebsstelle_2015 betriebsstelle2015(@PathVariable String abk) {
        return new Betriebsstelle_2015(BetriebsstellenREST.convertedData.get(abk.toUpperCase()));
    }

    /**
     * Alternative, Rückgabevariante, durch welche eine automatisch generierte
     * Antwort basierend auf der 2016-2018er-Version geliefert wird.
     *
     * @param abk - Abkürzung der Betriebsstelle
     * @return Betriebsstelle als Objekt in 2016_2018er-Variante
     */
    @GetMapping("/betriebsstelle2016_2018/{abk}")
    public Betriebsstelle_2016_2018 betriebsstelle2016_2018(@PathVariable String abk) {
        return new Betriebsstelle_2016_2018(BetriebsstellenREST.convertedData.get(abk.toUpperCase()));
    }

    /**
     * Alternative, Rückgabevariante, durch welche eine automatisch generierte
     * Antwort basierend auf der 2021er-Version geliefert wird.
     *
     * @param abk - Abkürzung der Betriebsstelle
     * @return Betriebsstelle als Objekt in 2021er-Variante
     */
    @GetMapping("/betriebsstelle2021/{abk}")
    public Betriebsstelle_2021 betriebsstelle2021(@PathVariable String abk) {
        return new Betriebsstelle_2021(BetriebsstellenREST.convertedData.get(abk.toUpperCase()));
    }

    /**
     * Fängt NullPointerExceptions ab, die zumeist durch Suche nach nicht
     * vorhandenen Abkürzungen erzeugt werden.
     * @return ResponseEntity mit Fehlernachricht.
     */
    @ExceptionHandler({NullPointerException.class})
    public ResponseEntity<String> handleNullPointer() {
        return new ResponseEntity<>("HTTP-STATUS: 500\n" +
                "Eine Betriebsstelle mit dieser Abkürzung konnte"
                + " nicht gefunden werden.", HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
