package DB.rest;

import org.json.simple.JSONObject;

/**
 * Allgemeiner, versionsunabhänbgiger Container für Betriebsstellen
 *
 * @author Marcel Gehrmann
 */
public class Betriebsstelle {

    private final JSONObject content;

    /**
     * Konstruktor für versionsunabhängige Betriebsstellen-Objekte
     *
     * @param content Daten der Betriebsstelle
     */
    public Betriebsstelle(JSONObject content) {
        this.content = content;
    }

    /**
     * @return content
     */
    public JSONObject getContent() {
        return content;
    }
}
