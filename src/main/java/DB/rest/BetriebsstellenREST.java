package DB.rest;

import java.util.ArrayList;
import java.util.HashMap;
import org.json.simple.JSONObject;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Implementierung zum Einlesen von Daten zu Betriebsstellen aus .csv-Dateien
 * und Bereitstellung dieser über einen REST-Endpoint.
 *
 * @author Marcel Gehrmann
 */
@SpringBootApplication
public class BetriebsstellenREST {

    public static HashMap<String, JSONObject> convertedData = new HashMap<>();

    /**
     * Hauptroutine zum Einlesen, Konvertieren und Bereitstellen von Daten zu
     * Betriebsstellen über REST-Endpoint.
     *
     * @param args
     */
    public static void main(String[] args) {
        switch (args.length) {
            case 1:
                CSVReader csvReader = new CSVReader(args[0]);
                csvReader.readCSV();
                ArrayList<String[]> data = csvReader.getStorage();
                Converter converter = new Converter(data);
                convertedData = converter.convert();
                SpringApplication.run(BetriebsstellenREST.class, args);
                break;
            case 0:
                System.out.println("Keine Datei zum Einlesen angegeben.");
                break;
            default:
                System.out.println("Zu viele Argumente angegeben.");
                break;
        }
    }
}
