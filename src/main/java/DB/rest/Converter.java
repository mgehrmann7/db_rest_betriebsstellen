/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DB.rest;

import java.util.ArrayList;
import java.util.HashMap;
import org.json.simple.JSONObject;

/**
 * Konvertierer für Betriebsstellen-CSV-Dateien.
 * @author Marcel Gehrmann
 */
public class Converter {

    private final ArrayList<String[]> storage;
    
    /**
     * Konstruktor für Converter-Objekte.
     * @param storage - Container für zu konvertierende Daten.
     */
    public Converter(ArrayList<String[]> storage) {
        this.storage = storage;
    }
    
    /**
     * Konvertiert Daten aus .csv-Dateien für Betriebsstellen
     * @return Daten aus der Variable 'storage', konvertiert zu Hashmap mit
     *  Abkürzungen als Key und restlichen Daten als Entry.
     */
    public HashMap <String, JSONObject> convert() {
        HashMap<String, JSONObject> convertedStorage = new HashMap<>();
        String[] header = storage.get(0);
        
        String pKey = null;
        for (String category : header) {
            if (category.equals("Abk")) {
                pKey = "Abk";
                break;
            }
            if (category.equals("RL100-Code")) {
                pKey = "RL100-Code";
                break;
            }
        }
        for (int i=1; i<storage.size(); i++) {
            JSONObject item = new JSONObject();
            String[] row = storage.get(i);
            for (int j=0; j<row.length; j++) {
                item.put(header[j], row[j]);
            }
            convertedStorage.put((String) item.get(pKey), item);
            convertedStorage.get(item.get(pKey)).remove(pKey);
        }
        
        return convertedStorage;
    }
}
