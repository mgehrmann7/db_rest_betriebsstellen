package DB.rest;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import org.apache.commons.io.input.BOMInputStream;

/**
 * Klasse zum Einlesen von .csv-Dateien mit Daten zu Betriebsstellen.
 *
 * @author Marcel Gehrmann
 */
public final class CSVReader {

    private String filePath;
    private BufferedReader reader;
    private ArrayList<String[]> storage;

    /**
     * Konstruktor für CSVReader.
     *
     * @param filePath - Pfad zur einzulesenden .csv-Datei.
     */
    public CSVReader(String filePath) {
        setFilePath(filePath);
        this.reader = null; // Initialize reader
    }

    /**
     * Funktion zum einlesen von .csv-Dateien. Öffnet den Reader und liest die
     * Daten zeilenweise ein. Bei Abschluss des Leseprozesses oder Fehler wird
     * der Reader geschlossen.
     */
    public void readCSV() {
        openReader();
        storage = new ArrayList<>(); // Reset storage
        String row = null; // Initialize rows
        try {
            while ((row = reader.readLine()) != null) {
                String[] data = row.split(";");
                storage.add(data);
            }
        } catch (IOException e) {
            System.out.print(e);
        } finally {
            closeReader();
        }
    }

    /**
     * @return die eingelesenen Daten
     */
    public ArrayList<String[]> getStorage() {
        return storage;
    }

    /**
     * Setzt neuen Wert für den Pfad zur einzulesenden .csv-Datei.
     *
     * @param newPath
     */
    public void setFilePath(String newPath) {
        this.filePath = newPath;
    }

    /**
     * Öffnet den Reader. Der Reader geht von Kodierung mit UTF-8 aus.
     */
    private void openReader() {
        try {
            InputStream inputStream = new FileInputStream(filePath);
            BOMInputStream bOMInputStream = new BOMInputStream(inputStream);
            this.reader = new BufferedReader(new InputStreamReader(
                    bOMInputStream, Charset.forName("UTF-8")));
        } catch (IOException e) {
            System.out.print(e);
        }
    }

    /**
     * Schließt den Reader.
     */
    private void closeReader() {
        try {
            this.reader.close();
        } catch (IOException e) {
            System.out.print(e);
        }
    }
}
