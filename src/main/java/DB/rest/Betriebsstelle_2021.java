package DB.rest;

import org.json.simple.JSONObject;

/**
 * Variante eines Betriebsstellen-Objekts nach 2021er Version.
 *
 * @author Marcel Gehrmann
 */
public class Betriebsstelle_2021 {

    private final String PLC;
    private final String RL100_Code;
    private final String RL100_Langname;
    private final String RL100_Kurzname;
    private final String Typ_Kurz;
    private final String Typ_Lang;
    private final String Betriebszustand;
    private final String Datum_ab;
    private final String Datum_bis;
    private final String Niederlassung;
    private final String Regionalbereich;
    private final String Letzte_Änderung;

    /**
     * Konstruktor für Betriebsstellen-Objekte nach 2021er Version.
     *
     * @param content Daten der Betriebsstelle
     */
    public Betriebsstelle_2021(JSONObject content) {
        this.PLC = (String) content.get("PLC");
        this.RL100_Code = (String) content.get("RL100-Code");
        this.RL100_Langname = (String) content.get("RL100-Langname");
        this.RL100_Kurzname = (String) content.get("RL100_Kurzname");
        this.Typ_Kurz = (String) content.get("Typ Kurz");
        this.Typ_Lang = (String) content.get("Typ Lang");
        this.Betriebszustand = (String) content.get("Betriebszustand");
        this.Datum_ab = (String) content.get("Datum ab");
        this.Datum_bis = (String) content.get("Datum bis");
        this.Niederlassung = (String) content.get("Niederlassung");
        this.Regionalbereich = (String) content.get("Regionalbereich");
        this.Letzte_Änderung = (String) content.get("Letzte Änderung");
    }

    /**
     * @return PLC
     */
    public String getPLC() {
        return PLC;
    }

    /**
     * @return RL100_Code
     */
    public String getRL100_Code() {
        return RL100_Code;
    }

    /**
     * @return RL100_Langname
     */
    public String getRL100_Langname() {
        return RL100_Langname;
    }

    /**
     * @return RL100_Kurzname
     */
    public String getRL100_Kurzname() {
        return RL100_Kurzname;
    }

    /**
     * @return Typ_Kurz
     */
    public String getTyp_Kurz() {
        return Typ_Kurz;
    }

    /**
     * @return Typ_Lang
     */
    public String getTyp_Lang() {
        return Typ_Lang;
    }

    /**
     * @return Betriebszustand
     */
    public String getBetriebszustand() {
        return Betriebszustand;
    }

    /**
     * @return Datum_ab
     */
    public String getDatum_ab() {
        return Datum_ab;
    }

    /**
     * @return Datum_bis
     */
    public String getDatum_bis() {
        return Datum_bis;
    }

    /**
     * @return Niederlassung
     */
    public String getNiederlassung() {
        return Niederlassung;
    }

    /**
     * @return Regionalbereich
     */
    public String getRegionalbereich() {
        return Regionalbereich;
    }

    /**
     * @return Letzte_Änderung
     */
    public String getLetzte_Änderung() {
        return Letzte_Änderung;
    }

}
