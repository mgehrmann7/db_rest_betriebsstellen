package DB.rest;

import org.json.simple.JSONObject;

/**
 * Variante eines Betriebsstellen-Objekts nach 2015er Version.
 *
 * @author Marcel Gehrmann
 */
public class Betriebsstelle_2015 {

    private final String Gültig_ab;
    private final String Ländercode;
    private final String Kurzname;
    private final String Locationcode;

    /**
     * Konstruktor für Betriebsstellen-Objekte nach 2015er Version.
     *
     * @param content Daten der Betriebsstelle
     */
    public Betriebsstelle_2015(JSONObject content) {
        this.Kurzname = (String) content.get("Kurzname");
        this.Ländercode = (String) content.get("L�ndercode");
        this.Locationcode = (String) content.get("Locationcode");
        this.Gültig_ab = (String) content.get("G�ltig ab");
    }

    public String getKurzname() {
        return Kurzname;
    }

    /**
     * @return Gültig_ab
     */
    public String getGültig_ab() {
        return Gültig_ab;
    }

    /**
     * @return Ländercode
     */
    public String getLändercode() {
        return Ländercode;
    }

    /**
     * @return Locationcode
     */
    public String getLocationcode() {
        return Locationcode;
    }
}
