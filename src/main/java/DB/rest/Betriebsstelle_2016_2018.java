package DB.rest;

import org.json.simple.JSONObject;

/**
 * Variante eines Betriebsstellen-Objekts nach 2016-2018er Version.
 *
 * @author Marcel Gehrmann
 */
public class Betriebsstelle_2016_2018 {

    private final String Name;
    private final String Kurzname;
    private final String Typ;
    private final String Betr_Zust;
    private final String Primary_location_code;
    private final String UIC;
    private final String RB;
    private final String Gültig_von;
    private final String Gultig_bis;
    private final String Netz_Key;
    private final String Fpl_rel;
    private final String Fpl_Gr;

    /**
     * Konstruktor für Betriebsstellen-Objekte nach 2016-2018er Version.
     *
     * @param content Daten der Betriebsstelle
     */
    public Betriebsstelle_2016_2018(JSONObject content) {
        this.Name = (String) content.get("Name");
        this.Kurzname = (String) content.get("Kurzname");
        this.Typ = (String) content.get("Typ");
        this.Betr_Zust = (String) content.get("Betr-Zust");
        this.Primary_location_code = (String) content.get("Primary location code");
        this.UIC = (String) content.get("UIC");
        this.RB = (String) content.get("RB");
        this.Gültig_von = (String) content.get("gültig von");
        this.Gultig_bis = (String) content.get("gültig bis");
        this.Netz_Key = (String) content.get("Netz-Key");
        this.Fpl_rel = (String) content.get("Fpl-rel");
        this.Fpl_Gr = (String) content.get("Fpl-Gr");
    }

    /**
     * @return Name
     */
    public String getName() {
        return Name;
    }

    /**
     * @return Kurzname
     */
    public String getKurzname() {
        return Kurzname;
    }

    /**
     * @return Typ
     */
    public String getTyp() {
        return Typ;
    }

    /**
     * @return Betr_Zust
     */
    public String getBetr_Zust() {
        return Betr_Zust;
    }

    /**
     * @return Primary_location_code
     */
    public String getPrimary_location_code() {
        return Primary_location_code;
    }

    /**
     * @return UIC
     */
    public String getUIC() {
        return UIC;
    }

    /**
     * @return RB
     */
    public String getRB() {
        return RB;
    }

    /**
     * @return gültig_von
     */
    public String getGültig_von() {
        return Gültig_von;
    }

    /**
     * @return gultig_bis
     */
    public String getGultig_bis() {
        return Gultig_bis;
    }

    /**
     * @return Netz_Key
     */
    public String getNetz_Key() {
        return Netz_Key;
    }

    /**
     * @return Fpl_rel
     */
    public String getFpl_rel() {
        return Fpl_rel;
    }

    /**
     * @return Fpl_Gr
     */
    public String getFpl_Gr() {
        return Fpl_Gr;
    }
}
