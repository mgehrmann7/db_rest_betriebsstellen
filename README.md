Dieses Programm kann genutzt werden, um CSV-Dateien mit Informationen zu Betriebsstellen einzulesen und über einen REST-Endpoint abzurufen.

Beim Start des Programms muss als Argument der Pfad der einzulesenden Datei angegeben werden
Beispielaufruf: java -jar DB_rest-1.0 "CSV-Datei"

Das Programm geht im jetzigen Zustand von einer Kodierung mit UTF-8 aus, kann jedoch angepasst werden, um die Kodierung automatisch zu bestimmen.

Die Anfrage zu Betriebsstellen funktioniert über deren Abkürzung bzw. RL100-Code.
Beispielanfrage: http://localhost:8080/betriebsstelle/aamp

Die Rückgabe der Daten der Betriebsstelle erfolgt als JSON-Objekt. Die zur Anfrage genutzte Abkürzung ist dabei derzeit nicht enthalten, lässt sich jedoch über eine simple Änderung hinzufügen.
